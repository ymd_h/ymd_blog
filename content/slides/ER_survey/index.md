---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Survey of Experience Replay"
summary: ""
authors: [ymd]
tags: [reinforcement_learning]
categories: []
date: 2021-01-25T19:05:19+09:00
slides:
  # Choose a theme from https://github.com/hakimel/reveal.js#theming
  theme: serif
  # Choose a code highlighting style (if highlighting enabled in `params.toml`)
  #   Light style: github. Dark style: dracula (default).
  highlight_style: github
---

# Survey of Experience Replay

Hiroyuki Yamada

---

## List of Papers Surveyed

* [H. Liu _et. al._, "Competitive Experience Replay", ICLR
  (2019)](https://iclr.cc/Conferences/2019/Schedule?showEvent=1091)
  ([arXiv](https://arxiv.org/abs/1902.00528))
* [G. Novati and P. Koumoutsakos, "Remember and Forget for Experience
  Replay", ICML
  (2019)](http://proceedings.mlr.press/v97/novati19a.html)
  ([arXiv](https://arxiv.org/abs/1807.05827),
  [code](https://github.com/cselab/smarties))
* [D. Zha _et. al._, "Experience Replay Optimization", IJCAI (2019)
  4243-4249](https://www.ijcai.org/Proceedings/2019/589)
  ([arXiv](https://arxiv.org/abs/1906.08387))
* [P. Sun _et. al._, "Attentive Experience Replay", AAAI (2020) 34,
  5900-5907](https://ojs.aaai.org//index.php/AAAI/article/view/6049)
* [J. Luo _et. al._, "Dynamic Experience Replay", CoRL
  (2020)](http://proceedings.mlr.press/v100/luo20a.html)
  ([arXiv](https://arxiv.org/abs/2003.02372))
* [Y. Oh _et. al._,
  "Learning to Sample with Local and Global Contexts in Experience Replay Buffer",
  ICLR (2021)](https://openreview.net/forum?id=gJYlaqL8i8)
  ([arXiv](https://arxiv.org/abs/2007.07358))

---

## 1. Competitive Experience Replay (CER)
[H. Liu _et. al._, "Competitive Experience Replay", ICLR (2019)](https://iclr.cc/Conferences/2019/Schedule?showEvent=1091) ([arXiv](https://arxiv.org/abs/1902.00528))

---

## 1. Competitive Experience Replay (CER)
[H. Liu _et. al._, "Competitive Experience Replay", ICLR (2019)](https://iclr.cc/Conferences/2019/Schedule?showEvent=1091) ([arXiv](https://arxiv.org/abs/1902.00528))

* For goal-oriented sparse reward task, CER improves HER. (Use together)
* Train a pair of agent $\pi _A$ and $\pi _B$ together
* Reward re-labelling for mini-batch
  * $r^i _A \leftarrow r^i _A -1$ if ${}^\exists j, |s^i _A - s^j _B| < \delta$
  * $r^j _B \leftarrow r^j _B +N$ where $N$ is the number of $s^i _A$ satisfying $|s^i _A - s^j _B| < \delta$
* Two types of initializations for $\pi _B$
  * indepentent-CER: $s^0 _B \sim p(s^0)$ (Usual task's initial distribution)
  * interect-CER: $s^0 _B \sim p(s^i _A)$ (Random off-policy sample of $\pi _A$)
---

## 1. Competitive Experience Replay (CER)
[H. Liu _et. al._, "Competitive Experience Replay", ICLR (2019)](https://iclr.cc/Conferences/2019/Schedule?showEvent=1091) ([arXiv](https://arxiv.org/abs/1902.00528))

![](ICLR(2019)_Fig1.png)

![](ICLR(2019)_Fig2.png)

* HER+CER superiors others

---

## 1. Competitive Experience Replay (CER)
[H. Liu _et. al._, "Competitive Experience Replay", ICLR (2019)](https://iclr.cc/Conferences/2019/Schedule?showEvent=1091) ([arXiv](https://arxiv.org/abs/1902.00528))

![](ICLR(2019)_Fig3.png)

* HER+CER superiors others

---

## 2. Remember and Forget for Experience Replay (ReF-ER)
[G. Novati and P. Koumoutsakos, "Remember and Forget for Experience Replay", ICML (2019)](http://proceedings.mlr.press/v97/novati19a.html) ([arXiv](https://arxiv.org/abs/1807.05827), [code](https://github.com/cselab/smarties))

---

## 2. Remember and Forget for Experience Replay (ReF-ER)
[G. Novati and P. Koumoutsakos, "Remember and Forget for Experience Replay", ICML (2019)](http://proceedings.mlr.press/v97/novati19a.html) ([arXiv](https://arxiv.org/abs/1807.05827), [code](https://github.com/cselab/smarties))

* Define Importance as $\rho _t= \frac{\pi (a_t \mid s_t)}{\mu _t (a_t \mid s_t)}$ where $\pi$: current policy $\mu _t$: behavior policy.
* If $\frac{1}{c_{\text{max}}} < \rho _t < c _{\text{max}} $, then
  "near-policy", otherwise "far-policy"
* For "far-policy", gradient is clipped to 0: $\hat{g}(w) \to 0$
* Penalize "off-policyness" by KL divergence: $\hat{g}^D(w) = \mathbb{E} [\nabla D_{\text{KL}}(\mu _k(\cdot\mid s_k)\| \pi ^w (\cdot \mid s_k))]$
* Total gradients: $\hat{g}^{\text{ReF-ER}} = \beta \hat{g}(w) - (1-\beta) \hat{g}^D(w)$
    * Anealing parameter: $\beta \leftarrow \cases{ (1-\eta) \beta & if $ \frac{n_{\text{far}}}{N} < D $ \cr (1-\eta)\beta + \eta & otherwise}$
    * $\eta$: NN learning rate
    * $n_{\text{far}}$: the number of "far-policy"
    * $N$: the number of transitions in Replay Buffer
    * $D$: hyper parameter

---

## 2. Remember and Forget for Experience Replay (ReF-ER)
[G. Novati and P. Koumoutsakos, "Remember and Forget for Experience Replay", ICML (2019)](http://proceedings.mlr.press/v97/novati19a.html) ([arXiv](https://arxiv.org/abs/1807.05827), [code](https://github.com/cselab/smarties))


![](PMLR(2019)97_4851-4860_Fig1.png)

* Results for DDPG with ReF-ER
* The authors says "replacing ER with ReF-ER stabilizes DDPG and
  greatly improves its performance, especially for tasks with complex
  dynamics (e.g.  Humanoid and Ant)"
* The authors also executed NAF with ReF-ER, and V-PACER with ReF-ER

---

## 3. Experience Replay Optimization (ERO)
[D. Zha _et. al._, "Experience Replay Optimization", IJCAI (2019) 4243-4249](https://www.ijcai.org/Proceedings/2019/589) ([arXiv](https://arxiv.org/abs/1906.08387))

---

## 3. Experience Replay Optimization (ERO)
[D. Zha _et. al._, "Experience Replay Optimization", IJCAI (2019) 4243-4249](https://www.ijcai.org/Proceedings/2019/589) ([arXiv](https://arxiv.org/abs/1906.08387))

![](IJCAI(2019)4243-4249_Fig1.png)

1. Replay Policy infers mask probabilities using features extracted from transition:  $\boldsymbol{\lambda} = \lbrace \phi (\mathbf{f}_{\mathcal{B} _i}\mid \theta ^{\phi}) \mid \mathcal{B} _i \in \mathbb{R}^{N} \rbrace$
1. Replay Buffer masks with Bernoulli distribution: $\mathbf{I} \sim \text{Bernoulli}(\boldsymbol{\lambda})$
1. Agent trains with transitions uniformly sampled from masked Replay Buffer
1. Replay Policy trains with:
    1. Replay Reward (difference of culmutive episode reward): $r^r = r^{c}_{\pi} - r^{c} _{\pi ^{\prime}}$
    1. $\nabla _{\theta ^{\phi}} \mathcal{J} \approx \sum _ {j:\mathcal{B}_j \in B^{\text{batch}}} r^{r} \nabla _{\theta ^{\phi}} [\mathbf{I}_j \log \phi + (1-\mathbf{I}_j)\log (1-\phi)] $

---

## 3. Experience Replay Optimization (ERO)
[D. Zha _et. al._, "Experience Replay Optimization", IJCAI (2019) 4243-4249](https://www.ijcai.org/Proceedings/2019/589) ([arXiv](https://arxiv.org/abs/1906.08387))

![](IJCAI(2019)4243-4249_Fig2.png)

* Average return of 5 times runs
* The authors say "ERO consistently outperforms all the baselines on
  most of the continuous control tasks in terms of sample efficiency"

---

## 4. Attentive Experience Replay (AER)
[P. Sun _et. al._, "Attentive Experience Replay", AAAI (2020) 34, 5900-5907](https://ojs.aaai.org//index.php/AAAI/article/view/6049)

---

## 4. Attentive Experience Replay (AER)
[P. Sun _et. al._, "Attentive Experience Replay", AAAI (2020) 34, 5900-5907](https://ojs.aaai.org//index.php/AAAI/article/view/6049)

* Sample $\lambda \times k$ transitions from Replay Buffer uniformly
* Calculate (task dependent) similarity $\mathcal{F}(s_j,s_t)$
  * For MuJoCo: $\mathcal{F}(s_1,s_2) = \frac{s_1\cdot s_2}{\| s_1\| \|s_2 \|}$
  * For Atari 2600: $\mathcal{F}(s_1,s_2) = - \| \phi (s1) - \phi (s_2) \|_2$
* Select most similar $k$ samples as mini-batch
* Aneal $\lambda$ linearly: $\lambda _0 \to 1$ within $\alpha \cdot T$ steps
  * $T$: total steps
  * $\alpha < 1$: hyper parameter


---

## 4. Attentive Experience Replay (AER)
[P. Sun _et. al._, "Attentive Experience Replay", AAAI (2020) 34, 5900-5907](https://ojs.aaai.org//index.php/AAAI/article/view/6049)


![](AAAI(2020)34_5900-5907_Fig3.png)

---

## 4. Attentive Experience Replay (AER)
[P. Sun _et. al._, "Attentive Experience Replay", AAAI (2020) 34, 5900-5907](https://ojs.aaai.org//index.php/AAAI/article/view/6049)


![](AAAI(2020)34_5900-5907_Fig4.png)
![](AAAI(2020)34_5900-5907_Fig6.png)

---


## 5. Dynamic Experience Replay (DER)
[J. Luo _et. al._, "Dynamic Experience Replay", CoRL (2020)](http://proceedings.mlr.press/v100/luo20a.html) ([arXiv](https://arxiv.org/abs/2003.02372))


---


## 5. Dynamic Experience Replay (DER)
[J. Luo _et. al._, "Dynamic Experience Replay", CoRL (2020)](http://proceedings.mlr.press/v100/luo20a.html) ([arXiv](https://arxiv.org/abs/2003.02372))

![](CoRL(2020)DER_Fig2.png)

* DER auguments human demonstrations by utilizing succsessful transisions
* Multiple Replay Buffers $\lbrace \mathbb{B} _0 \dots\mathbb{B} _n \rbrace$
  with demonstration zone
* On episode end, worker stores transitions into one of the replay
  buffers $\mathbb{B}_i$ randomly picked up.
* If the episode succeeds, these transitions are stored to anothor
  replay buffer $\mathbb{T}$, too.
* Trainer randomly picks one of the buffers $\mathbb{B}_j$, then
  samples, trains, and updates priorities.
* Periodically, demonstration zones in $\mathbb{B}_k$ are replaced by
  random samples of successful transitions $\mathbb{T}$.

---

## 5. Dynamic Experience Replay (DER)
[J. Luo _et. al._, "Dynamic Experience Replay", CoRL (2020)](http://proceedings.mlr.press/v100/luo20a.html) ([arXiv](https://arxiv.org/abs/2003.02372))

![](CoRL(2020)DER_Fig4.png)

---

## 5. Dynamic Experience Replay (DER)
[J. Luo _et. al._, "Dynamic Experience Replay", CoRL (2020)](http://proceedings.mlr.press/v100/luo20a.html) ([arXiv](https://arxiv.org/abs/2003.02372))

![](CoRL(2020)DER_Fig5.png)

---

## 6. Neural Experience Replay Sampler (NERS)

[Y. Oh _et. al._, "Learning to Sample with Local and Global Contexts in Experience Replay Buffer",ICLR (2021)](https://openreview.net/forum?id=gJYlaqL8i8) ([arXiv](https://arxiv.org/abs/2007.07358))

---

## 6. Neural Experience Replay Sampler (NERS)
[Y. Oh _et. al._, "Learning to Sample with Local and Global Contexts in Experience Replay Buffer",ICLR (2021)](https://openreview.net/forum?id=gJYlaqL8i8) ([arXiv](https://arxiv.org/abs/2007.07358))

![](ICLR(2021)_Algo1.png)

* NERS is NN which learns sampling score $\sigma _i$ from replay buffer
* RL policy is trained like PER except using inferred $\sigma _i$ instead of $\mid \text{TD}\mid$ for priority
* NERS is trained for replay reward: $r ^{\text{re}} = \sum _{t\in \text{current episode}} r _t - \sum _{t \in \text{previous episode}}r _t$
  * At episode end, choose subset of indices $I_{\text{train}}$ used for policy update
  * Update NERS with $\nabla _{\phi} \mathbb{E} _{I _\text{train}} [r^{\text{re}}] = \mathbb{E} _{I _\text{train}}\left [ \sum \nabla _{\phi} \sigma _i (D(I _{\text{train}})) \right ]$


---

## 6. Neural Experience Replay Sampler (NERS)
[Y. Oh _et. al._, "Learning to Sample with Local and Global Contexts in Experience Replay Buffer",ICLR (2021)](https://openreview.net/forum?id=gJYlaqL8i8) ([arXiv](https://arxiv.org/abs/2007.07358))


![](ICLR(2021)_Fig2.png)


* NERS consists 3 networks, local network $f _l$, global network $f _g$, and score network $f _s$
* Input features $D(I) = \lbrace s _{\kappa(i)}, a _{\kappa(i)}, r _{\kappa(i)}, s _{\kappa(i)+1}, \kappa(i), \delta _{\kappa(i)}, r _{\kappa(i)} + \gamma \max _a Q _{\hat {\theta}} (s _{\kappa(i)+1},a) \rbrace _ {i \in I}$
  * $\kappa (i)$: time step, $\delta _{\kappa (i)}$: TD error, $Q _{\hat {\theta}}$: target network
* Output of $f _g$ are averaged and concatenated to each output of $f _l$
* $f _s$ infer final score $\sigma _i$ from the concatenated local-global features.


---

## 6. Neural Experience Replay Sampler (NERS)
[Y. Oh _et. al._, "Learning to Sample with Local and Global Contexts in Experience Replay Buffer", ICLR (2021)](https://openreview.net/forum?id=gJYlaqL8i8) ([arXiv](https://arxiv.org/abs/2007.07358))


![](ICLR(2021)_Fig3.png)

---

## 6. Neural Experience Replay Sampler (NERS)
[Y. Oh _et. al._, "Learning to Sample with Local and Global Contexts in Experience Replay Buffer", ICLR (2021)](https://openreview.net/forum?id=gJYlaqL8i8) ([arXiv](https://arxiv.org/abs/2007.07358))


![](ICLR(2021)_Fig4.png)

---

## 6. Neural Experience Replay Sampler (NERS)
[Y. Oh _et. al._, "Learning to Sample with Local and Global Contexts in Experience Replay Buffer", ICLR (2021)](https://openreview.net/forum?id=gJYlaqL8i8) ([arXiv](https://arxiv.org/abs/2007.07358))


![](ICLR(2021)_Fig5.png)


---

# End

Thank You for Reading
