---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "cpprb"
summary: "Replay buffer classes for reinforcement learning."
authors: ["ymd"]
tags: ["Python","Cython","reinforcement_learning","cpprb"]
categories: []
date: 2020-02-02T21:16:41+09:00

# Optional external URL for project (replaces project detail page).
external_link:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: "Smart"
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
links:
- name: Project Site
  url: https://ymd_h.gitlab.io/cpprb/
  icon_pack: fas
  icon: home
- name: API Reference
  url: https://ymd_h.gitlab.io/cpprb/api/index.html
  icon_pack: fas
  icon: bookmark
- name: GitLab
  url: https://gitlab.com/ymd_h/cpprb
  icon_pack: fab
  icon: gitlab
- name: GitHub
  url: https://github.com/ymd-h/cpprb
  icon_pack: fab
  icon: github
- name: PyPI
  url: https://pypi.org/project/cpprb/
  icon_pack: fas
  icon: cubes

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---

cpprb is a python
([CPython](https://github.com/python/cpython/tree/master/Python))
module providing replay buffer classes for reinforcement learning.

Major target users are researchers and library developers.

You can build your own reinforcement learning algorithms together with
your favorite deep learning library
(e.g. [TensorFlow](https://www.tensorflow.org/),
[PyTorch](https://pytorch.org/)).

cpprb forcuses speed, flexibility, and memory efficiency.

By utilizing [Cython](https://cython.org/), complicated calculations
(e.g. segment tree for prioritized experience replay) are offloaded
onto C++.  (The name cpprb comes from "C++ Replay Buffer".)

In terms of API, initially cpprb referred to [OpenAI
Baselines](https://github.com/openai/baselines)' implementation. In
the current version, cpprb has much more flexibility. Any
[NumPy](https://numpy.org/) compatible types of any numbers of values
can be stored (as long as memory capacity is sufficient). For example,
you can store the next action and the next next observation, too.
