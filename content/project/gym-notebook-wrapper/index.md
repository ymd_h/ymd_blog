---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Gym Notebook Wrapper"
summary: "Wrapper for running and rendering OpenAI Gym on Jupyter Notebook"
authors: ["ymd"]
tags: ["Python","reinforcement_learning","gnwrapper","Jupyter_Notebook"]
categories: []
date: 2020-06-21T11:34:24+09:00

# Optional external URL for project (replaces project detail page).
external_link:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
links:
- name: GitLab
  url: https://gitlab.com/ymd_h/gym-notebook-wrapper
  icon_pack: fab
  icon: gitlab
- name: GitHub
  url: https://github.com/ymd-h/gym-notebook-wrapper
  icon_pack: fab
  icon: github
- name: PyPI
  url: https://pypi.org/project/gym-notebook-wrapper/
  icon_pack: fas
  icon: cubes

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---

Gym-Notebook-Wrapper provides wrapper classes for running and
rendering [OpenAI Gym](https://github.com/openai/gym) on [Jupyter
Notebook](https://jupyter.org/) or similar (e.g. [Google
Colab](https://colab.research.google.com/)).
