---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "gpu-array-js"
summary: "NDArray on WebGPU"
authors: ["ymd"]
tags: ["JavaScript", "WebGPU"]
categories: []
date: 2024-02-19T07:28:19+09:00

# Optional external URL for project (replaces project detail page).
external_link: ""

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
links:
- name: GitHub
  url: https://github.com/ymd-h/gpu-array-js
  icon_pack: fab
  icon: github

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---

gpu-array-js is a JavaScript PoC project providing NDArray (aka. multidimensional array) on WebGPU.

