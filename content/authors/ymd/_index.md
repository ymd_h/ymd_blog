---
# Display name
name: Hiroyuki Yamada

# Username (this should match the folder name)
authors:
- ymd

# Is this the primary user of the site?
superuser: true

# Role/position
role: ""

# Organizations/Affiliations
organizations:
- name: ""
  url: ""

# Short bio (displayed in user profile at end of posts)
bio: My research interests include machine learning, cloud native conputing.

interests:
- Artificial Intelligence
- Cloud Native Computing
- C++
- Python


# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: gitlab
  icon_pack: fab
  link: https://gitlab.com/ymd_h
- icon: github
  icon_pack: fab
  link: https://github.com/ymd-h
- icon: kaggle
  icon_pack: fab
  link: https://www.kaggle.com/ymdhryk

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Researchers
---

Hiroyuki Yamada got interested in IT (including programming, PC, etc.)
during his graduate school. A few years later, he became an IT
researcher at a Japanese company.

Encouraged by a friend, he started development of reinforcement
learning software [cpprb](https://gitlab.com/ymd_h/cpprb) using
[Cython](https://cython.org/) as a hobby at the beginning of 2019.
This continuing development is still one of the biggest hobby in his
life.

The author started this personal blog at the end of 2018. The
motivation was to learn the way of making a site by utilizing [GitLab
CI/CD](https://docs.gitlab.com/ee/ci/) and a static site generator
[Hugo](https://gohugo.io/). Additionaly, he wanted to organize his own
knowledge and experience based on the cpprb development.

[![H. Yamada's GitHub Stats](https://github-readme-stats.vercel.app/api?username=ymd-h&show_icons=true)](https://github.com/ymd-h)
